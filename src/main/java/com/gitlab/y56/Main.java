package com.gitlab.y56;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.Arrays;

public class Main {

  @SuppressWarnings("unchecked")
  public static void main(String[] args) {
    // read a file from jar
    InputStream is = Main.class.getResourceAsStream("/basin_tw97.geojson");
    BufferedReader br = new BufferedReader(new InputStreamReader(is));

    //JSON parser object to parse read file
    JSONParser jsonParser = new JSONParser();

    try {
      // Read JSON file
      Object obj = jsonParser.parse(br);
      JSONObject jsonObject = (JSONObject) obj;
      JSONArray basinList = (JSONArray) jsonObject.get("features");

      // the basin we want to select, by "BASIN_NO"
      String[] targetBasinNo = Arrays.copyOfRange(args, 1, args.length);

      basinList.removeIf(basin -> !isSelected(basin, targetBasinNo)); //  The basinList has been trimmed.

      JSONObject output = new JSONObject();
      output.put("type", "FeatureCollection");
      output.put("name", "SelectedC2560_tw97");
      output.put("features", basinList);

      // put crs info manually
      // "crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:EPSG::3826" } }
      JSONObject namexxxx = new JSONObject();
      namexxxx.put("name","urn:ogc:def:crs:EPSG::3826");

      JSONObject propertiesnamexxxx = new JSONObject();
      propertiesnamexxxx.put("properties", namexxxx);

      JSONObject crsxxxx = new JSONObject();
      crsxxxx.put("type", "name");
      crsxxxx.put("properties", namexxxx);

      output.put("crs", crsxxxx);

      FileWriter file = new FileWriter("./"+args[0]);
      file.write(output.toJSONString());
      file.flush();
      file.close();

      br.close();
      is.close();

      // print those input not found
      String[] whatWeGot = new String[targetBasinNo.length];
      basinList.forEach(basin -> getWhatWeGot(basin, whatWeGot));

      printFoundAndNot(targetBasinNo, whatWeGot);

    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ParseException e) {
      e.printStackTrace();
    }
  }
  private static boolean isSelected(Object basin, String[] targetBasinNo) {

    JSONObject geojsonBasin = (JSONObject) basin;
    JSONObject geojsonBasinProperties = (JSONObject) geojsonBasin.get("properties");
    String geojsonBasinPropertiesBasin_no = (String) geojsonBasinProperties.get("BASIN_NO");

    boolean found = false;
    int i = 0;
    while (!found && i < targetBasinNo.length) {
      if (targetBasinNo[i].equals(geojsonBasinPropertiesBasin_no)) {
        found = true;
      }
      i++;
    }
    return found;
  }
  private static String[] getWhatWeGot(Object basin, String[] collectResults) {

    JSONObject geojsonBasin = (JSONObject) basin;
    JSONObject geojsonBasinProperties = (JSONObject) geojsonBasin.get("properties");
    String geojsonBasinPropertiesBasin_no = (String) geojsonBasinProperties.get("BASIN_NO");

    int i  = 0;
    boolean putItIn = false;

    while ( !putItIn &&  i < collectResults.length) {
      if (collectResults[i] == null) {
        collectResults[i] = geojsonBasinPropertiesBasin_no;
        putItIn = true;
      }
      i++;
    }
    return collectResults;
  }
  private static void printFoundAndNot(String[] whatWeWant, String[] whatWeGot) {

    for (int i = 0; i < whatWeWant.length; i++) {

      boolean weGotIt = false;

      for (int j = 0; j < whatWeGot.length; j++) {

        if (whatWeWant[i].equals(whatWeGot[j]))
          weGotIt = true;
      }
      if (weGotIt) {
        System.out.print(whatWeWant[i]);
        System.out.println(" is found");
      }
      else {
        System.out.print(whatWeWant[i]);
        System.out.println(" is absent");
      }
    }
  }
}
