development note
https://hackmd.io/vZwdSkR6ThO3tTkN8PtjiA

# Select basins from `basin_tw97.geojson`

## log

- 1.2-SNAPSHOT
    - args[0] as output file path
        - e.g., `./workspace/selected.geojson`
            - starting at current working dir 
            - will not create dir for you
            - have to specify filename
            - no default path and name, you have to keyin sth
    - args[1~n] as `BASIN_NO`s we want
        - e.g., `./workspace/selected.geojson 1050 2650`

## The format of `basin_tw97.geojson`
After shrinking the content of `"coordinates"`, its reduced and beatified content is    
```json=
{
"type":     "FeatureCollection",
"name":     "C2560_tw97",
"features": [
              {
                "type": "Feature",
                "properties": {
                                "BASIN_NO": "2560",
                                "BASIN_NAME": "蘭陽溪",
                                "AREA": 1014975849.5469,
                                "BASIN_ID": 1021,
                                "EXTENT": "281364.88,2696023.00,333926.94,2747870.50"
                              },
                "geometry": {
                              "type": "Polygon",
                              "coordinates":  [ [
                                                  [ 327469.519885070330929, 2739754.274718830827624 ],
                                                  [ 327509.082774622191209, 2739765.017264634370804 ]
                                              ] ]
                            }
              }
            ]
}
```
### Windows cmd example
```
C:\Users\GG\IdeaProjects\selectbasin>"C:\Program Files\Java\jdk-12\bin\java.exe" -Dfile.encoding=utf-8 -jar C:\Users\GG\IdeaProjects\selectbasin\out\artifacts\selectbasin_jar\selectbasin.jar 2560 1050 1850 1540 2350
```
### Linus terminal example
```
/usr/lib/jvm/jdk-12.0.1/bin/java -Dfile.encoding=UTF-8 -jar /home/y56/h2beaver-public-y56/select-basin/out/artifacts/selectbasin_jar/selectbasin.jar 1010
```

## After manually adding "crs"
```json=
{
   "features":[
      {
         "geometry":{
            "coordinates":[ ],
            "type":"Polygon"
         },
         "type":"Feature",
         "properties":{
            "BASIN_NAME":"社皆坑溪",
            "AREA":1.34082205176E7,
            "EXTENT":"218040.02,2444659.00,222169.75,2450035.00",
            "BASIN_NO":"1842",
            "BASIN_ID":5044
         }
      },
      {
         "geometry":{
            "coordinates":[ ],
            "type":"Polygon"
         },
         "type":"Feature",
         "properties":{
            "BASIN_NAME":"磺溪",
            "AREA":4.94879062773E7,
            "EXTENT":"303678.41,2784165.50,314694.09,2794295.00",
            "BASIN_NO":"1010",
            "BASIN_ID":2001
         }
      }
   ],
   "crs":{
      "type":"name",
      "properties":{
         "name":"urn:ogc:def:crs:EPSG::3826"
      }
   },
   "name":"SelectedC2560_tw97",
   "type":"FeatureCollection"
}
```

